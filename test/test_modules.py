import unittest
import json
from ml4proflow import modules
from ml4proflow_mods.multiprocessing.modules import DFGProcessSinkModule, \
                                                    killexceptionhook


class TestModulesModule(unittest.TestCase):
    def setUp(self):
        self.dfm = modules.DataFlowManager()

    def tearDown(self):
        killexceptionhook(None, None, None)

    def test_create_new_DFGProcessSinkModule(self):
        self.dfm.create_channel('src')
        m_config = {'max_queue_size': 4,
                    'process_count': 1,
                    'channels_pull': ['src'],
                    'subgraph_channel': 'sub_src',
                    'subgraph_config': {},
                    }
        dut = DFGProcessSinkModule(self.dfm, m_config)
        self.assertIsInstance(dut, DFGProcessSinkModule)
        dut.on_shutdowning()
        dut.on_shutdowned()

    def test_create_dfGraph_2(self):
        # Gen sub graph:
        push_channels = ['sub_src']
        dfg_sub_desc = []
        for i in range(2):
            pull_channels = push_channels
            push_channels = ['pass_through_%d' % i]
            dfg_sub_desc.append({
                "module_ident": "ml4proflow.modules_extra.PassThroughModule",
                "module_config": {
                    'channels_push': push_channels,
                    'channels_pull': pull_channels
                    }
                })
        dfg_sub_desc.append({
            "module_ident": "ml4proflow.modules_extra.StatisticSinkModule",
            "module_config": {'channels_pull': push_channels}
            })
        dfg_sub_desc = {'modules': dfg_sub_desc}
        dfg = {'modules': [{
                "module_ident": "ml4proflow.modules_extra"
                                + ".RepeatingSourceModule",
                "module_config": {'channels_push': ['src']}
                }, {
                "module_ident": "ml4proflow_mods.multiprocessing"
                                + ".modules.DFGProcessSinkModule",
                "module_config": {'max_queue_size': 64,
                                  'process_count': 32,
                                  'channels_pull': ['src'],
                                  'subgraph_channel': 'sub_src',
                                  'subgraph_config': dfg_sub_desc,
                                  },
                }]}
        dut_dfg = modules.DataFlowGraph(self.dfm, dfg)
        with open('1k-64buf-32cpus.json', 'w') as f:
            json.dump(dut_dfg.get_config(), f)
        dut_dfg.shutdown()

    def test_create_dfGraph_2_process(self):
        # gen sub graph:
        push_channels = ['sub_src']
        dfg_sub_desc = []
        for i in range(2):
            pull_channels = push_channels
            push_channels = ['pass_through_%d' % i]
            dfg_sub_desc.append({
                "module_ident": "ml4proflow.modules_extra.PassThroughModule",
                "module_name": "",
                "module_config": {
                    'channels_push': push_channels,
                    'channels_pull': pull_channels
                    }
                })
        dfg_sub_desc.append({
            "module_ident": "ml4proflow.modules_extra.StatisticSinkModule",
            "module_config": {'channels_pull': push_channels}})
        dfg_sub_desc = {'modules': dfg_sub_desc}
        dfg = {'modules': [{
                "module_ident": "ml4proflow.modules_extra"
                                + ".RepeatingSourceModule",
                "module_config": {'channels_push': ['src']}
                }, {
                "module_ident": "ml4proflow_mods.multiprocessing"
                                + ".modules.DFGProcessSinkModule",
                "module_config": {'max_queue_size': 4,
                                  'process_count': 1,
                                  'channels_pull': ['src'],
                                  'subgraph_channel': 'sub_src',
                                  'subgraph_config': dfg_sub_desc,
                                  },
                },
                ]}
        dut_dfg = modules.DataFlowGraph(self.dfm, dfg)
        dut_dfg.execute_once()
        dut_dfg.shutdown()


if __name__ == '__main__':  # pragma: no cover
    unittest.main()
