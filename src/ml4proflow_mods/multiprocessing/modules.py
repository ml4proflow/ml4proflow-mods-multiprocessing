from __future__ import annotations
from multiprocessing import Process, JoinableQueue, Lock
import multiprocessing
from typing import Any
import sys
import time
import traceback
from pandas import DataFrame
from traceback import print_exception
from ml4proflow.modules import SourceModule, SinkModule, DataFlowManager, \
                               DataFlowGraph
import psutil


def worker(q: JoinableQueue, config: dict[str, Any], print_lock: Lock) -> None:
    try:
        dfm = DataFlowManager()
        dfm.create_channel(config['subgraph_channel'])
        graph = DataFlowGraph(dfm, config['subgraph_config'])
        for m in graph.modules:
            m.config['print_lock'] = print_lock
        while True:
            data = q.get()
            if data is None:
                break  # we call task_done later!
            # TODO: what should we do with the src argument!?!
            dfm.new_data(config['subgraph_channel'], None, data)
            q.task_done()
        graph.on_shutdowning()
        graph.on_shutdowned()
        q.task_done()  # !one more task_done needed for the stop item!
    except Exception:
        print("Warning child got an exception!!!")
        traceback.print_exc()
        # try to mark the task as done,
        # just in case the exception happened before the call
        try:
            q.task_done()
        except ValueError:
            pass
        # clear work queue to propagate error to main process
        while not q.empty():
            q.get()
            q.task_done()


def killexceptionhook(exctype, value, traceback) -> None:
    print('Warning the kill ExceptionHook got called!!!')
    print('Your processing pipeline contains multiprococessing modules')
    print_exception(exctype, value, traceback)
    print("Killing child procs!")
    for p in multiprocessing.active_children():
        p.kill()
    for p in multiprocessing.active_children():
        p.terminate()
    print('Done killing')


class DFGProcessSinkModule(SinkModule):
    def __init__(self, dfm: DataFlowManager, config: dict[str, Any]):
        SinkModule.__init__(self, dfm, config)
        self.exception = False
        sys.excepthook = killexceptionhook
        config.setdefault('process_count', 1)  # TODO: autodetect cpus
        self.queue: JoinableQueue = JoinableQueue(config['max_queue_size'])
        self._print_lock = Lock()
        self.pool = [
            Process(target=worker, args=(self.queue, config, self._print_lock))
            for i in range(config['process_count'])
            ]
        for p in self.pool:
            p.start()
        parent = psutil.Process()
        for child in parent.children():
            child.nice(15)

    def on_new_data(self, name: str, sender: SourceModule,
                    data: DataFrame) -> None:
        self.queue.put(data)

    def on_shutdowning(self) -> None:
        self.queue.join()
        super().on_shutdowning()

    def on_shutdowned(self) -> None:
        for p in self.pool:
            self.queue.put(None)
        # No join here, that would block on child errors
        self.queue.close()
        time.sleep(1)  # wait 1s for child processes
        for p in self.pool:
            if p.is_alive():
                print("Warning there are still processes alive!")
                p.kill()
        super().on_shutdowned()
